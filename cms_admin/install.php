<?php
require_once ('header.php');
?>
<body>
<div class="container">
    <h2 class="mt-5">Setup database</h2>
    <form action="install-script.php" method="post">
        <div class="form-group">
            <label for="host">Hostname</label>
            <input type="text" class="form-control" id="host" name="host" placeholder="localhost" required>
        </div>
        <div class="form-group">
            <label for="dbname">Database name</label>
            <input type="text" class="form-control" id="dbname" name="dbname" placeholder="localhost" required>
        </div>
        <div class="form-group">
            <label for="user">Username</label>
            <input type="text" class="form-control" id="user" name="user" placeholder="Database username" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <h2 class="mt-5">Site details</h2>
        <div class="form-group">
            <label for="siteTitle">Site title</label>
            <input type="text" class="form-control" id="siteTitle" name="siteTitle" placeholder="Site title" required>
        </div>
        <div class="form-group">
            <label for="siteUrl">Site url</label>
            <input type="text" class="form-control" id="siteUrl" name="siteUrl" placeholder="Site url" required>
            <small id="urlHelper" class="form-text text-muted">Complete site url like http://www.mysite.com</small>
        </div>
        <div class="form-group">
            <label for="siteContactEmail">Contact email</label>
            <input type="email" class="form-control" id="siteContactEmail" name="siteContactEmail" placeholder="Contact email" required>
        </div>

        <button class="btn btn-primary">Install</button>
    </form>
</div>
</body>
</html>
