<?php
include_once ('header.php');
include_once('../include/pageService.php');

$contentService = new PageService();

if($_SERVER["REQUEST_METHOD"] == "POST"){

    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $content = $_POST['content'];
    $slug = filter_input(INPUT_POST, 'slug', FILTER_SANITIZE_STRING);

    if(isset($_POST["save"])){
        $page = array('name' => $name, 'title' => $title, 'description' => $description, 'content' => $content, 'slug' => $slug );
        $contentAdd = $contentService->addPage($page);
    }
    if(isset($_POST["update"])){
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $page = array('id' => $id, 'name' => $name, 'title' => $title, 'description' => $description, 'content' => $content, 'slug' => $slug );
        $contentAdd = $contentService->addPage($page);
    }


    $GLOBALS['contentAdd'] = $contentAdd;

    if($contentAdd){
        header("Location: index.php");
        exit;
    } else{
        echo 'Error try again!';
    }

}

if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["delete"])){
    $id = filter_input(INPUT_GET, 'delete', FILTER_SANITIZE_NUMBER_INT);
    $contentService->deleteOne($id);
    header("Location: index.php");
    exit;
}

if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["edit"])){
    $id = filter_input(INPUT_GET,'edit', FILTER_SANITIZE_NUMBER_INT);
    $page = $contentService->getOne($id);

}

?>
<div class="container mt-5">
    <?php if(isset($_GET["edit"])): ?>
    <h2>Edit page</h2>
    <?php else: ?>
    <h2>Add page</h2>
    <?php endif ?>
<form action="add-edit-page.php" method="post">
    <?php if(isset($_GET["edit"])): ?>
    <div class="form-group">
        <label for="id">Page name</label>
        <input type="number" class="form-control" id="id" name="id" placeholder="DB id" value="<?php if(isset($_GET["edit"])) echo $page->getId() ?>" hidden>
    </div>
    <?php endif ?>
    <div class="form-group">
        <label for="name">Page name</label>
        <input type="text" class="form-control" id="name" name="name" value="<?php if(isset($_GET["edit"])) echo $page->getName() ?>" placeholder="Page name" required>
    </div>
    <div class="form-group">
        <label for="title">Page title</label>
        <input type="text" class="form-control" id="title" name="title" value="<?php if(isset($_GET["edit"])) echo $page->getTitle() ?>" placeholder="Page title" required>
    </div>
    <div class="form-group">
        <label for="description">Page short description</label>
        <input type="text" class="form-control" id="description" name="description" value="<?php if(isset($_GET["edit"])) echo $page->getShortDescription() ?>" placeholder="Short description used for meta description" required>
    </div>
    <div class="form-group">
        <label for="content">Page content</label>
        <textarea class="form-control" id="content" name="content" rows="10" placeholder="Enter HTML code" required> <?php if(isset($_GET["edit"])) echo $page->getContent() ?> </textarea>
    </div>
    <div class="form-group">
        <label for="slug">Page slug</label>
        <input type="text" class="form-control" id="slug" name="slug" value="<?php if(isset($_GET["edit"])) echo $page->getCleanUrl() ?>" placeholder="Short url" pattern="([a-zA-Z0-9,-]*)" required>
    </div>
    <?php if(isset($_GET["edit"])): ?>
    <button class="btn btn-primary" name="update">Edit page</button>
    <?php else: ?>
    <button class="btn btn-primary" name="save">Add page</button>
    <?php endif ?>
</form>
</div>



<?php include_once ('footer.php'); ?>

