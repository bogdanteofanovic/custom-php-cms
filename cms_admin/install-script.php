<?php
require_once ('header.php');
$host = filter_input(INPUT_POST,'host', FILTER_SANITIZE_STRING);
$user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);
$pass = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
$dbName = filter_input(INPUT_POST, 'dbname', FILTER_SANITIZE_STRING);

$siteUrl = filter_input(INPUT_POST, 'siteUrl', FILTER_SANITIZE_STRING);
$siteTitle = filter_input(INPUT_POST, 'siteTitle', FILTER_SANITIZE_STRING);
$siteContactEmail = filter_input(INPUT_POST, 'siteContactEmail', FILTER_SANITIZE_STRING);

$sql = "


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema pico_cms
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema pico_cms
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `$dbName` DEFAULT CHARACTER SET utf8 ;
USE `$dbName` ;

-- -----------------------------------------------------
-- Table `pico_cms`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `$dbName`.`user` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `username` NVARCHAR(50) NOT NULL,
  `password` TEXT NOT NULL,
  `email` NVARCHAR(50) NULL,
  `role` NVARCHAR(50) NOT NULL,
  `active` TINYINT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = '	';


-- -----------------------------------------------------
-- Table `pico_cms`.`menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `$dbName`.`menu` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(150) NOT NULL,
  `content` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pico_cms`.`language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `$dbName`.`language` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `langCode` NVARCHAR(10) NOT NULL,
  `langHomePageId` BIGINT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_language_page1_idx` (`langHomePageId` ASC) VISIBLE,
  CONSTRAINT `fk_language_page1`
    FOREIGN KEY (`langHomePageId`)
    REFERENCES `$dbName`.`page` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pico_cms`.`page`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `$dbName`.`page` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(100) NOT NULL,
  `title` NVARCHAR(100) NOT NULL,
  `shortDescription` NVARCHAR(300) NULL,
  `content` TEXT NOT NULL,
  `userId` BIGINT NOT NULL,
  `active` TINYINT NOT NULL,
  `cleanUrl` NVARCHAR(150) NOT NULL,
  `menuId` BIGINT NOT NULL,
  `languageId` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_page_user1_idx` (`userId` ASC) VISIBLE,
  UNIQUE INDEX `cleanUrl_UNIQUE` (`cleanUrl` ASC) VISIBLE,
  INDEX `fk_page_menu1_idx` (`menuId` ASC) VISIBLE,
  INDEX `fk_page_language1_idx` (`languageId` ASC) VISIBLE,
  CONSTRAINT `fk_page_user1`
    FOREIGN KEY (`userId`)
    REFERENCES `$dbName`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_page_menu1`
    FOREIGN KEY (`menuId`)
    REFERENCES `$dbName`.`menu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_page_language1`
    FOREIGN KEY (`languageId`)
    REFERENCES `$dbName`.`language` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pico_cms`.`cmsData`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `$dbName`.`cmsData` (
  `siteUrl` NVARCHAR(300) NOT NULL,
  `siteTitle` NVARCHAR(150) NOT NULL,
  `siteContactEmail` NVARCHAR(100) NULL,
  `defaultLanguageId` BIGINT NOT NULL,
  INDEX `fk_cmsData_language1_idx` (`defaultLanguageId` ASC) VISIBLE,
  CONSTRAINT `fk_cmsData_language1`
    FOREIGN KEY (`defaultLanguageId`)
    REFERENCES `$dbName`.`language` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `pico_cms`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `$dbName`;
INSERT INTO `$dbName`.`user` (`id`, `username`, `password`, `email`, `role`, `active`) VALUES (1, 'bogdan', 'bogdan', 'bogdan@pico.rs', 'admin', 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `pico_cms`.`menu`
-- -----------------------------------------------------
START TRANSACTION;
USE `$dbName`;
INSERT INTO `$dbName`.`menu` (`id`, `name`, `content`) VALUES (1, 'serbian', 'meni srpski');
INSERT INTO `$dbName`.`menu` (`id`, `name`, `content`) VALUES (2, 'english', 'menu english');
INSERT INTO `$dbName`.`menu` (`id`, `name`, `content`) VALUES (3, 'german', 'menu german');

COMMIT;


-- -----------------------------------------------------
-- Data for table `pico_cms`.`language`
-- -----------------------------------------------------
START TRANSACTION;
USE `$dbName`;
INSERT INTO `$dbName`.`language` (`id`, `langCode`, `langHomePageId`) VALUES (1, 'sr', NULL);
INSERT INTO `$dbName`.`language` (`id`, `langCode`, `langHomePageId`) VALUES (2, 'en', NULL);
INSERT INTO `$dbName`.`language` (`id`, `langCode`, `langHomePageId`) VALUES (3, 'de', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `pico_cms`.`page`
-- -----------------------------------------------------
START TRANSACTION;
USE `$dbName`;
INSERT INTO `$dbName`.`page` (`id`, `name`, `title`, `shortDescription`, `content`, `userId`, `active`, `cleanUrl`, `menuId`, `languageId`) VALUES (1, 'test stranica', 'Test sql stranica', 'Test kratak opis', 'Test sadrzaj', 1, 1, 'test-serbian-stranica', 1, 1);
INSERT INTO `$dbName`.`page` (`id`, `name`, `title`, `shortDescription`, `content`, `userId`, `active`, `cleanUrl`, `menuId`, `languageId`) VALUES (2, 'english page', 'Page title', 'short page description', 'Dummy 2 content', 1, 1, 'test-english-page', 2, 2);
INSERT INTO `$dbName`.`page` (`id`, `name`, `title`, `shortDescription`, `content`, `userId`, `active`, `cleanUrl`, `menuId`, `languageId`) VALUES (3, 'german page', 'German test page', 'short german description', 'Dummy german content', 1, 1, 'test-german-page', 3, 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `pico_cms`.`cmsData`
-- -----------------------------------------------------
START TRANSACTION;
USE `$dbName`;
INSERT INTO `$dbName`.`cmsData` (`$siteUrl`, `$siteTitle`, `$siteContactEmail`, `defaultLanguageId`) VALUES ('http://localhost', 'Test web site', 'office@pico.rs', 1);

COMMIT;





";

try {
    $dbh = new PDO("mysql:host=$host", $user, $pass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// if your are not set this attributes you won't get any exceptions.
    $dbh->exec($sql);
    $message = "Database created! Enjoy";

    $define = "<?php
    define('DB_HOST','".$host."');
    define('DB_USER','".$user."');
    define('DB_PASS','".$pass."');
    define('DB_NAME','".$dbName."');";

    $confFile = ('../include/dbConfig.php');
    file_put_contents($confFile, $define);

} catch (PDOException $e) {
    $message = $e->getMessage();
}

$GLOBALS['message'] = $message;

?>
<body>
<div class="container text-center mt-5">
    <h1><?php echo $message ?></h1>
</div>
</body>
</html>
