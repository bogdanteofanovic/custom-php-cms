<?php
include_once('header.php');
include_once('../include/pageService.php');
include_once ('../include/menuService.php');
include_once('../include/cmsDataService.php');

$contentService = new PageService();
$menuService = new MenuService();
$cmsService = new CmsDataService();

$resultPages = $contentService->getAll();
$resultMenus = $menuService->getAll();
$cms = $cmsService->getCmsData();

$path = $_SERVER['REQUEST_URI'];

$GLOBALS['cms'] = $cms;
$GLOBALS['resultPages'] = $resultPages;
$GLOBALS['reslutMenus'] = $resultMenus;

?>
<body>
<div class="container-fluid mb-10 mt-5">

<div class="row">
    <div class="col-lg-6 col-md-12">

        <div class="card">
            <div class="card-header">
                <h2>Pages</h2>
            </div>
            <div class="card-body">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Title</th>
                        <th scope="col">Short description</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($resultPages as $r) { ?>

                        <tr>
                            <th scope="row" class="align-middle"><?php echo $r['id'] ?></th>
                            <td class="align-middle"><?php echo $r['name'] ?></td>
                            <td class="align-middle"><?php echo $r['title'] ?></td>
                            <td class="align-middle"><?php echo $r['shortDescription'] ?></td>
                            <td class="align-middle"><?php echo $r['cleanUrl'] ?></td>
                            <td class="align-middle"><a target="_blank" href="<?php echo $cms->getSiteUrl().'/'.$r['cleanUrl']?>" class="btn-sm btn-info w-100 mr-1"> <i class="fas fa-link"></i> </a> <a href="add-edit-page.php?edit=<?php echo $r['id'] ?>" class="btn-sm btn-secondary w-100 mr-1"><i class="fas fa-edit"></i></a><a href="add-edit-page.php?delete=<?php echo $r['id'] ?>" class="btn-sm btn-danger w-100"><i class="fas fa-trash-alt"></i></a></td>
                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="add-edit-page.php" class="btn btn-primary m-2">Add page</a>
            </div>
        </div>


    </div>
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-header">
                <h2>Menus</h2>
            </div>
            <div class="card-body">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Content</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($resultMenus as $r) { ?>

                        <tr>
                            <th scope="row" class="align-middle"><?php echo $r['id'] ?></th>
                            <td class="align-middle"><?php echo $r['name'] ?></td>
                            <td class="align-middle"><?php echo $r['content'] ?></td>
                            <td class="align-middle"><a href="add-edit-menu.php?edit=<?php echo $r['id'] ?>" class="btn-sm btn-secondary w-100 mr-1"><i class="fas fa-edit"></i></a><a href="add-edit-menu.php?delete=<?php echo $r['id'] ?>" class="btn-sm btn-danger w-100"><i class="fas fa-trash-alt"></i></a></td>
                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="#" class="btn btn-warning m-2">Add menu</a>
            </div>
        </div>



    </div>
</div>


</div>

<?php include_once ('footer.php') ?>