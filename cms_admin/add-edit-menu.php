<?php
include_once ('header.php');
include_once('../include/menuService.php');

$menuService = new MenuService();

if($_SERVER["REQUEST_METHOD"] == "POST"){

    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $content = $_POST['content'];

    if(isset($_POST["save"])){
        $menu = array('name' => $name, 'content' => $content );
        $menuAdd = $menuService->add($menu);
    }
    if(isset($_POST["update"])){
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $menu = array('id' => $id, 'name' => $name, 'content' => $content);
        $menuAdd = $menuService->add($menu);
    }


    $GLOBALS['menuAdd'] = $menuAdd;

    if($menuAdd){
        header("Location: index.php");
        exit;
    } else{
        echo 'Error try again!';
    }

}

if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["delete"])){
    $id = filter_input(INPUT_GET, 'delete', FILTER_SANITIZE_NUMBER_INT);
    $menuService->deleteOne($id);
    header("Location: index.php");
    exit;
}

if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["edit"])){
    $id = filter_input(INPUT_GET,'edit', FILTER_SANITIZE_NUMBER_INT);
    $menu = $menuService->getOne($id);

}

?>
<div class="container mt-5">
    <?php if(isset($_GET["edit"])): ?>
        <h2>Edit menu</h2>
    <?php else: ?>
        <h2>Add menu</h2>
    <?php endif ?>
    <form action="add-edit-menu.php" method="post">
        <?php if(isset($_GET["edit"])): ?>
            <div class="form-group">
                <label for="id">Page name</label>
                <input type="number" class="form-control" id="id" name="id" placeholder="DB id" value="<?php if(isset($_GET["edit"])) echo $menu->getId() ?>" hidden>
            </div>
        <?php endif ?>
        <div class="form-group">
            <label for="name">Menu name</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php if(isset($_GET["edit"])) echo $menu->getName() ?>" placeholder="Menu name" required>
        </div>
        <div class="form-group">
            <label for="content">Page content</label>
            <textarea class="form-control" id="content" name="content" rows="10" placeholder="Enter menu HTML code" required> <?php if(isset($_GET["edit"])) echo $menu->getContent() ?> </textarea>
        </div>
        <?php if(isset($_GET["edit"])): ?>
            <button class="btn btn-primary" name="update">Edit menu</button>
        <?php else: ?>
            <button class="btn btn-primary" name="save">Add menu</button>
        <?php endif ?>
    </form>
</div>



<?php include_once ('footer.php'); ?>

