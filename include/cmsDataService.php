<?php
include_once('dbUtils.php');
include_once('cmsDataModel.php');

class CmsDataService
{
    /**
     * cmsService constructor.
     */
    public function __construct()
    {
    }

    public function getCmsData(){
        $dummyData = new CmsData('http://dummysite','Dummy title','dummy@email.com',0,0);

        try{
            $dbUtils = new DBUtils();
            $conn = $dbUtils->getConnection();
            if($stmt = $conn->prepare("select * from cmsdata")){
                $stmt->execute();
                $stmt->bind_result($siteUrl,$siteTitle,$siteContactEmail,$defaultLanguageId);
                while ($stmt->fetch()) {
                    $tempData = new CmsData($siteUrl,$siteTitle,$siteContactEmail,$defaultLanguageId);
                }
                $stmt->close();
            }

        }catch(PDOException $e){
            $e->getMessage();
        }
        if(isset($tempData)){
            return $tempData;
        }
        else{
            return $dummyData;
        }

    }

}