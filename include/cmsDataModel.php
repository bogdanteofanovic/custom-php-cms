<?php


class CmsData
{
    private $siteUrl;
    private $siteTitle;
    private $siteContactEmail;
    private $defaultLanguageId;

    public function __construct($siteUrl, $siteTitle, $siteContactEmail, $defaultLanguageId)
    {
        $this->siteUrl = $siteUrl;
        $this->siteTitle = $siteTitle;
        $this->siteContactEmail = $siteContactEmail;

        $this->defaultLanguageId = $defaultLanguageId;

    }

    /**
     * @return mixed
     */
    public function getSiteUrl()
    {
        return $this -> siteUrl;
    }

    /**
     * @param mixed $siteUrl
     */
    public function setSiteUrl($siteUrl)
    {
        $this -> siteUrl = $siteUrl;
    }

    /**
     * @return mixed
     */
    public function getSiteTitle()
    {
        return $this -> siteTitle;
    }

    /**
     * @param mixed $siteTitle
     */
    public function setSiteTitle($siteTitle)
    {
        $this -> siteTitle = $siteTitle;
    }

    /**
     * @return mixed
     */
    public function getSiteContactEmail()
    {
        return $this -> siteContactEmail;
    }

    /**
     * @param mixed $siteContactEmail
     */
    public function setSiteContactEmail($siteContactEmail)
    {
        $this -> siteContactEmail = $siteContactEmail;
    }

    /**
     * @return mixed
     */

    /**
     * @return mixed
     */
    public function getDefaultLanguageId()
    {
        return $this -> defaultLanguageId;
    }

    /**
     * @param mixed $defaultLanguageId
     */
    public function setDefaultLanguageId($defaultLanguageId)
    {
        $this -> defaultLanguageId = $defaultLanguageId;
    }



}