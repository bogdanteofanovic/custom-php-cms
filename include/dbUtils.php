<?php
    /**
     * Content class for dbUtils
     */

    require_once ('dbConfig.php');


    class DBUtils {

        private $conn;
        private $servername;
        private $username;
        private $password;
        private $dbname;

        function __construct()
        {
            $this -> servername = DB_HOST;
            $this -> username = DB_USER;
            $this -> password = DB_PASS;
            $this -> dbname = DB_NAME;

        }
        public function getConnection(){

            if(isset($this->conn)){
                return $this->conn;
            }else{
                return new mysqli($this->servername, $this->username, $this->password, $this->dbname);
            }
        }
    }