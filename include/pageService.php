<?php
    require_once('dbUtils.php');
	require_once('pageModel.php');
	require_once ('menuModel.php');

	/**
	 * Content class for every page in pico cms
	 */
	class PageService
	{	
		
		
		function __construct()
		{

		}

		public function addPage($page){
            try{
                $dbUtils = new DBUtils();
                $conn = $dbUtils->getConnection();

                if(isset($page['id'])){
                    if($stmt = $conn->prepare("update page set name = ?, title = ?, shortDescription = ?, content = ?, userId = 1, active = 1, cleanUrl = ?, languageId = 1, menuId = 1 where id = ?")){
                        $stmt->bind_param("sssssi", $page['name'],$page['title'],$page['description'],$page['content'],$page['slug'], $page['id']);
                        $status = $stmt->execute();
                        if ($status === false) {
                            trigger_error($stmt->error, E_USER_ERROR);
                        }
                        $stmt->close();
                        return 2;
                    }

                }else{

                    if($stmt = $conn->prepare("insert into page (name, title, shortDescription, content, userId, active, cleanUrl, languageId) values (?,?,?,?,1,1,?,1)")){
                        $stmt->bind_param("sssss", $page['name'],$page['title'],$page['description'],$page['content'],$page['slug']);
                        $stmt->execute();
                        $stmt->close();
                        return 1;
                    }
                }


            }catch(PDOException $e){

                trigger_error( $e->getMessage(), E_USER_ERROR);
                return -1;
            }

        }

        public function deleteOne($id){
            try{
                $dbUtils = new DBUtils();
                $conn = $dbUtils->getConnection();
                if($stmt = $conn->prepare("delete from page where id = ?")){
                    $stmt->bind_param("i", $id);
                    $stmt->execute();
                    $stmt->close();
                }

            }catch(PDOException $e){
                $e->getMessage();
                return -1;
            }

        }

        public function getOne($id){
            try{
                $dbUtils = new DBUtils();
                $conn = $dbUtils->getConnection();
                if($stmt = $conn->prepare("select * from page where id = ?")){
                    $stmt->bind_param("i", $id);
                    $stmt->execute();
                    $stmt->bind_result($id,$name,$title,$shortDescription,$content,$userId,$active,$cleanUrl,$languageId, $menuId);
                    while ($stmt->fetch()) {
                        $tempContent = new Page($id,$name,$title,$shortDescription,$content,$cleanUrl);
                    }
                    $stmt->close();
                    return $tempContent;
                }

            }catch(PDOException $e){
                $e->getMessage();
                return -1;
            }

        }

        public function getAll(){
		    $result = array();
            try{
                $dbUtils = new DBUtils();
                $conn = $dbUtils->getConnection();
                if($stmt = $conn->prepare("select * from page")){
                    $stmt->execute();
                    $stmt->bind_result($id,$name,$title,$shortDescription,$content,$userId,$active,$cleanUrl,$languageId, $menuId);
                    while ($stmt->fetch()) {
                        $r = array('id' => $id, 'name' => $name, 'title' => $title, 'shortDescription' => $shortDescription, 'content' => $content, 'cleanUrl' => $cleanUrl );
                        array_push($result, $r);
                    }
                    $stmt->close();
                }

            }catch(PDOException $e){
                $e->getMessage();
                return -1;
            }
            if(isset($result)){
                return $result;
            }
            else{
                return -1;
            }

        }

        public function getLangRoot($langCode){
            $dummyContent = new Page(0,'Wrong page','Page not found!','Page not found 404', "Wrong url! Page not found on this link <a href='http://local'>sadasd</a>",0,1,'wrong-page',1);
            try{
                $dbUtils = new DBUtils();
                $conn = $dbUtils->getConnection();
                if($stmt = $conn->prepare("select page.id, page.name, page.title, page.shortDescription, page.content, page.cleanUrl, menu.id, menu.name, menu.content from language join page on langHomePageId = page.id join menu on page.menuId = menu.id where langCode =  ?")){
                    $stmt->bind_param("s", $langCode);
                    $stmt->execute();
                    $stmt->bind_result($id,$name,$title,$shortDescription,$content,$cleanUrl, $menu_Id, $menuName, $menuContent);
                    while ($stmt->fetch()) {
                        $tempContent = new Page($id,$name,$title,$shortDescription,$content,$cleanUrl);
                        $tempMenu = new Menu($menu_Id,$menuName,$menuContent);
                        $tempContent->setMenu($tempMenu);

                    }
                    $stmt->close();
                }
            }catch(PDOException $e){
                $e->getMessage();
                return -1;
            }
            if(isset($tempContent)){

                return $tempContent;
            }
            else{
                return $dummyContent;
            }
        }

		public function getBySlug($slug){
			$dummyContent = new Page(0,'Wrong page','Page not found!','Page not found 404', "Wrong url! Page not found on this link <a href='http://local'>sadasd</a>",0,1,'wrong-page',1);
			try{
                $dbUtils = new DBUtils();
				$conn = $dbUtils->getConnection();
				if($stmt = $conn->prepare("select * from page join menu on page.menuId = menu.id where cleanUrl=?")){
					$stmt->bind_param("s", $slug);
					$stmt->execute();
					$stmt->bind_result($id,$name,$title,$shortDescription,$content,$userId,$active,$cleanUrl,$menuId,$languageId, $menu_Id, $menuName, $menuContent);
					while ($stmt->fetch()) {
						$tempContent = new Page($id,$name,$title,$shortDescription,$content,$cleanUrl);
						$tempMenu = new Menu($menu_Id,$menuName,$menuContent);
						$tempContent->setMenu($tempMenu);

					}
					$stmt->close();
				}

			}catch(PDOException $e){
				$e->getMessage();
				return -1;
			}
			if(isset($tempContent)){

			    return $tempContent;
            }
			else{
			    return $dummyContent;
            }

		}

        public function isRoot(){
            $path = $_SERVER['REQUEST_URI'];
            if(!isset(explode("/",$path)[2]) || trim(explode("/",$path)[2]) === ''){
                return true;
            }else{
                return false;
            }
        }

        public function isLangRoot(){
            $path = $_SERVER['REQUEST_URI'];
            if(!isset(explode("/",$path)[3]) || trim(explode("/",$path)[3]) === ''){
                return true;
            }else{
                return false;
            }
        }

        public function getContentRoot(){
            try{
                $dbUtils = new DBUtils();
                $conn = $dbUtils->getConnection();
                if($stmt = $conn->prepare("select page.id as pageId,page.name as pageName,title,shortDescription,page.content as pageContent,cleanUrl, menu.id as menuId, menu.name as menuName, menu.content as menuContent from cmsdata join language on defaultLanguageId = language.id join page on langHomePageId = page.id join menu on page.menuId = menu.id")){
                    $stmt->execute();
                    $stmt->bind_result($id,$name,$title,$shortDescription,$content,$cleanUrl, $menuId, $menuName, $menuContent);
                    while ($stmt->fetch()) {
                        $tempContent = new Page($id,$name,$title,$shortDescription,$content,$cleanUrl);
                        $tempMenu = new Menu($menuId, $menuName, $menuContent);
                        $tempContent->setMenu($tempMenu);
                    }
                    $stmt->close();
                }

            }catch(PDOException $e){
                $e->getMessage();
            }
            if(isset($tempContent)){
                return $tempContent;
            }
            else{
                return null;
            }

        }

    }
