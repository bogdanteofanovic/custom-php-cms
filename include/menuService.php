<?php
include_once ('menuModel.php');
require_once('dbUtils.php');

class MenuService
{
    /**
     * MenuService constructor.
     */
    public function __construct()
    {
    }

    public function add($menu){
        try{
            $dbUtils = new DBUtils();
            $conn = $dbUtils->getConnection();

            if(isset($menu['id'])){
                if($stmt = $conn->prepare("update menu set name = ?, content = ? where id = ?")){
                    $stmt->bind_param("ssi", $menu['name'],$menu['content'], $menu['id']);
                    $status = $stmt->execute();
                    if ($status === false) {
                        trigger_error($stmt->error, E_USER_ERROR);
                    }
                    $stmt->close();
                    return 2;
                }

            }else{

                if($stmt = $conn->prepare("insert into menu (name, content) values (?,?)")){
                    $stmt->bind_param("ss", $menu['name'],$menu['content']);
                    $stmt->execute();
                    $stmt->close();
                    return 1;
                }
            }


        }catch(PDOException $e){

            trigger_error( $e->getMessage(), E_USER_ERROR);
            return -1;
        }

    }

    public function getOne($id){
        try{
            $dbUtils = new DBUtils();
            $conn = $dbUtils->getConnection();
            if($stmt = $conn->prepare("select * from menu where id = ?")){
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->bind_result($id,$name, $content);
                while ($stmt->fetch()) {
                    $tempContent = new Menu($id,$name,$content);
                }
                $stmt->close();
                return $tempContent;
            }

        }catch(PDOException $e){
            $e->getMessage();
            return -1;
        }

    }

    public function getAll(){
        $result = array();
        try{
            $dbUtils = new DBUtils();
            $conn = $dbUtils->getConnection();
            if($stmt = $conn->prepare("select * from menu")){
                $stmt->execute();
                $stmt->bind_result($id,$name, $content);
                while ($stmt->fetch()) {
                    $r = array('id' => $id, 'name' => $name, 'content' => $content );
                    array_push($result, $r);
                }
                $stmt->close();
            }

        }catch(PDOException $e){
            $e->getMessage();
            return -1;
        }
        if(isset($result)){
            return $result;
        }
        else{
            return -1;
        }

    }

    public function deleteOne($id){
        try{
            $dbUtils = new DBUtils();
            $conn = $dbUtils->getConnection();
            if($stmt = $conn->prepare("delete from menu where id = ?")){
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->close();
                return 1;
            }

        }catch(PDOException $e){
            $e->getMessage();
            return -1;
        }

    }


}