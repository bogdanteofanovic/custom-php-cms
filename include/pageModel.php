<?php

	/**
	 * Content class for all pages in pico cms
	 */
	class Page
	{
		private $id;
		private $name;
		private $title;
		private $shortDescription;
		private $content;
		private $userId;
		private $active;
		private $cleanUrl;
		private $languageId;
		private $menuId;
		private $menu;
		
		function __construct($id, $name, $title, $shortDescription, $content, $cleanUrl, $languageId = 1 , $menuId = 1, $userId = 1, $active = 1)
		{
			$this->id = $id;
			$this->name = $name;
			$this->title = $title;
			$this->shortDescription = $shortDescription;
			$this->content = $content;
			$this->userId = $userId;
			$this->active = $active;
			$this->cleanUrl = $cleanUrl;
			$this->languageId = $languageId;
			$this->menuId = $menuId;

		}

	    /**
	     * @return mixed
	     */
	    public function getId()
	    {
	        return $this->id;
	    }

	    /**
	     * @return mixed
	     */
	    public function getName()
	    {
	        return $this->name;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTitle()
	    {
	        return $this->title;
	    }

	    /**
	     * @return mixed
	     */
	    public function getShortDescription()
	    {
	        return $this->shortDescription;
	    }

	    /**
	     * @return mixed
	     */
	    public function getContent()
	    {
	        return $this->content;
	    }

	    /**
	     * @return mixed
	     */
	    public function getUserId()
	    {
	        return $this->userId;
	    }

	    /**
	     * @return mixed
	     */
	    public function getActive()
	    {
	        return $this->active;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCleanUrl()
	    {
	        return $this->cleanUrl;
	    }

        /**
     * @return mixed
     */
        public function getMenuId()
        {
            return $this -> menuId;
        }

        /**
         * @return mixed
         */
        public function getLanguageId()
        {
            return $this -> languageId;
        }

        /**
         * @return mixed
         */
        public function getMenu()
        {
            return $this -> menu;
        }

        /**
         * @param mixed $menu
         */
        public function setMenu($menu)
        {
            $this -> menu = $menu;
        }






	    
}
