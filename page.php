<?php

    include_once ('./include/cmsDataService.php');
    include_once ('./include/menuModel.php');
    include_once ('./include/pageService.php');

    $cmsService = new CmsDataService();

    $pageId = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_STRING);

    $lang = filter_input(INPUT_GET, 'lang', FILTER_SANITIZE_STRING);

	$menuService = new PageService();
	$content = $menuService->getBySlug($pageId);

	if($menuService->isRoot()){
        $content = $menuService->getContentRoot();
    }elseif ($menuService->isLangRoot() && isset($_GET['lang'])){
	    $content = $menuService->getLangRoot($lang);
    }
	else{
        $content = $menuService->getBySlug($pageId);
    }
    $pageMenu = $content->getMenu();

    //global varibale used in header and footer
    //$tempContent = $content['tempContent'];
	//$contentTemp = new Page($tempContent['id'], $tempContent['name'], $tempContent['title'],$tempContent['shortDescription'], $tempContent['content'], $tempContent['cleanUrl']);
	$GLOBALS['content'] = $content;

    require_once ('./include/header.php');
?>

<body>

<navbar class="fixed-top bg-dark text-white">
        <div class="container text-center  p-3">
            <a href="<?php echo $cmsService -> getCmsData() -> getSiteUrl()?>">Srpski</a>
            <a href="<?php echo $cmsService -> getCmsData() -> getSiteUrl().'/en/'?>">English</a>
            <a href="<?php echo $cmsService -> getCmsData() -> getSiteUrl().'/de/'?>">German</a>
        </div>
</navbar>

<div class="container mt-3 text-center mt-5">

    <h1><?php echo $content -> getTitle() ?></h1>
<p><?php echo  $content -> getShortDescription() ?></p>
    <?php echo $content -> getContent(); ?>
    <?php echo $pageMenu -> getContent(); ?>
</div>
</body>

<?php require_once ('./include/footer.php'); ?>
